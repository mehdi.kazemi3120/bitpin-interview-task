.phony: upload-stage

BUILD_VERSION ?= $(shell git describe --tags `git rev-list --tags --max-count=1`)

build:
	# @./env/bin/pip freeze > requirements.txt
	@docker build -t bitpin-back:${BUILD_VERSION} -f ./deployment/Dockerfile .

run:
	@BUILD_VERSION=${BUILD_VERSION} docker-compose -f ./deployment/docker-compose.yml up -d

