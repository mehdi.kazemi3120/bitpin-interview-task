from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
]

# API Version 1
urlpatterns += [
    path("api/v1/", include("blog.urls")),
]
