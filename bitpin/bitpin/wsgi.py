from django.core.wsgi import get_wsgi_application
import os
from dotenv import dotenv_values
from pathlib import Path

BASE_DIR = Path(__file__).resolve().parent.parent.parent
project_folder = os.path.expanduser(BASE_DIR)
config = dotenv_values(os.path.join(project_folder, '.env'))

development_mode = config["DEVELOPMENT_MODE"]

if development_mode == "TRUE":
    settings = "development"
else:
    settings = "production"


os.environ.setdefault('DJANGO_SETTINGS_MODULE',
                      f"bitpin.settings.{settings}")

application = get_wsgi_application()
