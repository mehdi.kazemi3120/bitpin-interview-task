from django.db import models
from django.utils.translation import gettext_lazy as _
import uuid


class BaseModel(models.Model):

    uuid = models.UUIDField(
        _("uuid"),
        default=uuid.uuid4,
        unique=True,
        help_text=_("Instance Unique ID"),
        editable=False
    )

    create_at = models.DateTimeField(
        _("create at"),
        editable=False,
        auto_now_add=True,
        help_text=_("Creation date and time"),
    )

    class Meta:
        abstract = True
