from rest_framework import serializers, status
from rest_framework.exceptions import APIException, NotFound
from django.core.exceptions import ObjectDoesNotExist
from .models import Post, Vote
from django.contrib.auth.models import User
from django.utils.translation import gettext_lazy as _
from django.db.models import Avg


class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ["id", "username"]


class VoteDetailSerializer(serializers.ModelSerializer):

    class Meta:
        model = Vote
        fields = ["id", "point"]


class PostListSerializer(serializers.ModelSerializer):

    user_votes_count = serializers.SerializerMethodField()
    average_point = serializers.SerializerMethodField()

    user_vote = serializers.SerializerMethodField()

    def get_user_vote(self, obj):
        current_user = self.context.get("request").user
        current_user_vote = current_user.votes.filter(post=obj)
        return VoteDetailSerializer(current_user_vote.first()).data if current_user_vote.exists() else None

    def get_user_votes_count(self, obj):
        return obj.votes.all().count()

    def get_average_point(self, obj):
        return obj.votes.all().aggregate(Avg('point'))['point__avg']

    class Meta:
        model = Post
        fields = ["id", "subject", "user_votes_count",
                  "average_point", "user_vote"]


class VoteSerializer(serializers.Serializer):
    point = serializers.IntegerField()

    def validate_point(self, point):
        if point > 5 or point < 0:
            raise serializers.ValidationError(
                _("Point number is not valid , Must be 0-5"))
        return point

    def create(self, validated_data):
        try:
            user = self.context.get("request").user
            vote, _ = Vote.objects.get_or_create(
                user=user,
                post=Post.objects.get(id=self.context.get("post_id"))
            )
            vote.point = validated_data['point']
            vote.save()
            return vote
        except ObjectDoesNotExist:
            response = NotFound('Post not found')
            raise response

        except Exception as e:
            response = APIException(detail=str(e))
            response.status_code = status.HTTP_400_BAD_REQUEST
            raise response
