from django.urls import path
from . import views as blog_view


urlpatterns = [

    path("posts/", blog_view.PostListView.as_view(), name="post_list_view"),
    path("posts/<int:post_id>/vote/",
         blog_view.VoteView.as_view(), name="vote_view"),
]
