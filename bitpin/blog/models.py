from django.db import models
from django.utils.translation import gettext_lazy as _
from django.contrib.auth.models import User
from utils.base_model import BaseModel


class Post(BaseModel):

    subject = models.CharField(
        _("post subject"),
        blank=True,
        null=True,
        max_length=100
    )

    text = models.TextField(
        _("post text"),
        blank=True,
        null=True,
    )

    def __str__(self):
        return self.subject

    class Meta:
        managed = True
        verbose_name = _("Post")
        verbose_name_plural = _("Posts")


class Vote(BaseModel):

    user = models.ForeignKey(
        User,
        verbose_name=_("Related User"),
        on_delete=models.CASCADE,
        related_name="votes",
        blank=True,
        null=True
    )

    post = models.ForeignKey(
        Post,
        verbose_name=_("Related Post"),
        on_delete=models.CASCADE,
        related_name="votes",
        blank=True,
        null=True
    )

    point = models.IntegerField(
        _("point"),
        blank=True,
        null=True
    )

    def __str__(self):
        return str(self.point)

    class Meta:
        managed = True
        verbose_name = _("Vote")
        verbose_name_plural = _("Votes")
