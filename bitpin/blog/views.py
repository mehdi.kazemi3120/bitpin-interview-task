from .models import Post
from django.utils.translation import gettext_lazy as _
from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from .serializers import PostListSerializer, VoteSerializer
from rest_framework.generics import ListAPIView, CreateAPIView
from rest_framework.response import Response
from rest_framework.pagination import PageNumberPagination


class PostListView(ListAPIView):
    permission_classes = [
        IsAuthenticated,
    ]
    http_method_names = ["get"]
    serializer_class = PostListSerializer
    pagination_class = PageNumberPagination

    def get_queryset(self):
        return Post.objects.all()

    def get(self, request, *args, **kwargs):
        page = self.paginate_queryset(self.get_queryset())

        serializer = self.get_paginated_response(
            self.serializer_class(
                page,
                many=True,
                context={'request': request}
            ).data
        )

        return Response(serializer.data, status=status.HTTP_200_OK)


class VoteView(CreateAPIView):
    permission_classes = [
        IsAuthenticated,
    ]
    http_method_names = ["post"]
    serializer_class = VoteSerializer
    lookup_field = 'post_id'

    def get_serializer_context(self):
        ctx = super().get_serializer_context()
        ctx.update(post_id=self.kwargs.get(self.lookup_field))
        return ctx

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(
            data=request.data, context=self.get_serializer_context())
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)
