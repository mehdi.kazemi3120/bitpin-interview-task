#!/usr/bin/env python
"""Django's command-line utility for administrative tasks."""
import os
import sys
from dotenv import dotenv_values
from pathlib import Path

BASE_DIR = Path(__file__).resolve().parent.parent
project_folder = os.path.expanduser(BASE_DIR)
config = dotenv_values(os.path.join(project_folder, '.env'))

development_mode = config["DEVELOPMENT_MODE"]

if development_mode == "TRUE":
    settings = "development"
else:
    settings = "production"


def main():
    """Run administrative tasks."""

    os.environ.setdefault('DJANGO_SETTINGS_MODULE',
                          f"bitpin.settings.{settings}")

    try:
        from django.core.management import execute_from_command_line
    except ImportError as exc:
        raise ImportError(
            "Couldn't import Django. Are you sure it's installed and "
            "available on your PYTHONPATH environment variable? Did you "
            "forget to activate a virtual environment?"
        ) from exc
    execute_from_command_line(sys.argv)


if __name__ == '__main__':
    main()
