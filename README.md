Development Mode :
    
    1 - pip install pipenv
    2 - pipenv install --dev
    3 - pipenv run ./bitpin/manage.py migrate
    4 - pipenv run ./bitpin/manage.py createsuperuser
    5 - pipenv run ./bitpin/manage.py runserver 0.0.0.0:8000


Production Mode With Docker :

    1 - docker volume create db_volume
    2 - cp .env.example .env
    3 - change .env file to DEVELOPMENT_MODE=FALSE
    4 - make build run
